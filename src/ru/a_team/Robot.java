package ru.a_team;

/**
 * <b>Класс Robot.</b>
 * <ul>Содержит в себе:
 * <li>Алгоритм поворота головы робота.</li>
 * <li>Начальные и конечные значения точек робота.</li>
 * <li>Вывод информации данных о роботе.</li></ul>
 *
 * @author Олшаускис Артур.
 */
public class Robot {

    private Point currentPoint = new Point();
    private View view;

    /**
     * Конструктор с параметрами.
     *
     * @param currentPoint - Хранит начальная координаты робота.
     * @param view         - Хранит повороты головы робота.
     */
    Robot(Point currentPoint, View view) {
        this.currentPoint = currentPoint;
        this.view = view;
    }

    /**
     * Конструктор по умолчанию.
     */
    Robot() {
        this.currentPoint = new Point();
        this.view = View.UP;
    }

    public Point getCurrentPoint() {
        return currentPoint;
    }

    public void setCurrentPoint(Point currentPoint) {
        this.currentPoint = currentPoint;
    }

    public void setView(View view) {
        this.view = view;
    }

    public View getView() {
        return view;
    }

    /**
     * Метод поворота головы влево.
     */
    void turnLeft() {
        switch (this.view) {
            case UP:
                this.view = View.LEFT;
                break;
            case LEFT:
                this.view = View.UP;
                break;
            case DOWN:
                this.view = View.RIGHT;
                break;
            case RIGHT:
                this.view = View.UP;
                break;
        }
    }

    /**
     * Метод поворота головы вправо.
     */
    void turnRight() {
        switch (this.view) {
            case UP:
                this.view = View.RIGHT;
                break;
            case DOWN:
                this.view = View.LEFT;
                break;
            case RIGHT:
                this.view = View.DOWN;
                break;
            case LEFT:
                this.view = View.UP;
                break;
        }
    }

    /**
     * Вывод информации по роботу.
     *
     * @return возвращает информацию по роботу (Координаты старта, прибытия и направления взгляда).
     */
    @Override
    public String toString() {
        return "Координаты точки, в которой находится робот: " + currentPoint +
                "\nНаправление взгляда: " + view;
    }

}