package ru.a_team;

/**
 * Перечисление "Направление взгляда", содержит метод установки направления взгляда и переопределение метода ToString.
 * @author Анохин Владимир
 */
public enum View {
    UP("UP"),
    DOWN("DOWN"),
    RIGHT("RIGHT"),
    LEFT("LEFT");

    private String view;

    View(String viewStr) {
        view = viewStr;
    }

    public String getView(){
        return view;
    }
}