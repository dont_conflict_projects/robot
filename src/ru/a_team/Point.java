package ru.a_team;

/**
 * Класс Point
 * Предназначен для обозначения координат опредленной точки.
 * Хранит в себе только целочисленные значения.
 *
 * @author Анохин Владимир.
 */
public class Point {

    private int x, y;

    /**
     * Конструктор с параметром
     *
     * @param x - первая точка.
     * @param y - вторая точка.
     */
    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Конструктор по умолчанию.
     */
    Point() {
        this.x = 0;
        this.y = 0;
    }

    int getX() {
        return x;
    }

    void setX(int x) {
        this.x = x;
    }

    int getY() {
        return y;
    }

    void setY(int y) {
        this.y = y;
    }

    /**
     * Вывод точек на экран.
     *
     * @return возвращает координаты в формате - {x,y}
     */
    @Override
    public String toString() {
        return "{" + x + "," + y + "}";
    }
}