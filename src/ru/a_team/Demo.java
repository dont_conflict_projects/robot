package ru.a_team;
import java.io.PrintWriter;

/**
 * <b>Демонстрационный класс.</b>
 * <ul><li>Демонстрирует передвижение робота по координатам.</li>
 * <li>Демонстритует изменения направления взгляда робота.</li>
 * <li>Содержит алгоритм по которому передвигается робот.</li>
 * <li>Выводит информацию в файл.</li>
 * <li>Вычисляет дошел ли робот до нужной точки или нет.</ul>
 *
 * @author - Колгатина Дарья.
 */

public class Demo {

    private static Robot robot = new Robot(new Point(-50, -45), View.UP);
    private static Point endPoint = new Point(11, -8);
    private static Point startPoint = new Point(robot.getCurrentPoint().getX(), robot.getCurrentPoint().getY());

    private static String moveHorizontal = "";
    private static String moveVertical = "";

    public static void main(String[] args) throws Exception {
        move(robot, robot.getCurrentPoint());
        writeFile();
    }

    /**
     * Метод перемещения робота.
     * @param robot - Объект-Робот.
     * @param currentPoint - Текущие положение робота.
     */
    private static void move(Robot robot, Point currentPoint) {
        int step = 0;
        /* Перемещения по горизонтали */
        if(currentPoint.getX() >= endPoint.getX()){
            /* Перемщение робота влево */
            switch (robot.getView()){
                case UP:
                    robot.turnLeft();
                    break;
                case DOWN:
                    robot.turnRight();
                    break;
                case RIGHT:
                    robot.turnLeft();
                    robot.turnLeft();
                    break;
            }
            moveHorizontal += "Взгляд робота повернулся: " + robot.getView() + "\n";
            while (currentPoint.getX() > endPoint.getX()) {
                currentPoint.setX(currentPoint.getX() - 1);
                step++;
            }
            moveHorizontal += "Прошёл по горизонтали " + step + " шагов. \n\n";
        } else {
            /* Перемщение робота вправо */
            switch (robot.getView()){
                case UP:
                    robot.turnRight();
                    break;
                case DOWN:
                    robot.turnLeft();
                    break;
                case LEFT:
                    robot.turnLeft();
                    robot.turnLeft();
                    break;
            }
            moveHorizontal += "Взгляд робота повернулся: " + robot.getView() + "\n";

            while (currentPoint.getX() < endPoint.getX()) {
                currentPoint.setX(currentPoint.getX() + 1);
                step++;
            }
            moveHorizontal += "Прошёл по горизонтали " + step + " шагов. \n\n";
        }


        step = 0;
        if(currentPoint.getY() >= endPoint.getY()) {
            /* Перемщение робота вниз */
            switch (robot.getView()){
                case RIGHT:
                    robot.turnRight();
                    break;
                case LEFT:
                    robot.turnLeft();
                    break;
                case UP:
                    robot.turnLeft();
                    robot.turnLeft();
                    break;
            }
            moveVertical += "Взгляд робота повернулся: " + robot.getView() + "\n";
            while (currentPoint.getY() > endPoint.getY()) {
                currentPoint.setY(currentPoint.getY() - 1);
                step++;
            }
            moveVertical += "Прошёл по вертикали " + step + " шагов. \n";

        } else {
            /* Перемщение робота вверх */
            switch (robot.getView()){
                case LEFT:
                    robot.turnRight();
                    break;
                case RIGHT:
                    robot.turnLeft();
                    break;
                case DOWN:
                    robot.turnLeft();
                    robot.turnLeft();
                    break;
            }
            moveVertical += "Взгляд робота повернулся: " + robot.getView() + "\n";

            while (currentPoint.getY() < endPoint.getY()) {
                currentPoint.setY(currentPoint.getY() + 1);
                step++;
            }
            moveVertical += "Прошёл по вертикали " + step + " шагов. \n\n";
        }
    }

    /**
     * Метод записи в файл.
     * Отправляет данные в файл.
     */
    private static void writeFile() throws Exception {

        PrintWriter out = new PrintWriter("RobotFile/result.txt");
        out.println("Начальные координаты робота: (" + startPoint.getX() + "; " + startPoint.getY() + ")");
        out.println("Заданное направление взгляда: " + robot.getView());
        out.println("Конечные координаты робота: (" + endPoint.getX() + "; " + endPoint.getY() + ")");
        out.println("---------------------------");
        out.print(moveHorizontal);
        out.print(moveVertical);
        out.println("---------------------------");
        out.println("Робот пришёл в точку: (" + robot.getCurrentPoint().getX() + "; " + robot.getCurrentPoint().getY() + ")");
        out.println("Робот дошёл!");

        out.close();
    }

}

